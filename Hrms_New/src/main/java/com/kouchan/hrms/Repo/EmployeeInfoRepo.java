package com.kouchan.hrms.Repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kouchan.hrms.model.EmployeeInfoModel;

@Repository
public interface EmployeeInfoRepo extends JpaRepository<EmployeeInfoModel, Integer> {

	@Query("SELECT m from EmployeeInfoModel m WHERE m.mobileNumber =?1")
	EmployeeInfoModel findByMobileNumber(String mobileNumber);

	@Query("SELECT m from EmployeeInfoModel m WHERE m.mobileNumber =?1 and m.password=?2")
	EmployeeInfoModel findByMobileNumberandPassword(String mobileNumber, String enteredPass);

	@Query("SELECT m from EmployeeInfoModel m WHERE m.mobileNumber =?1")
	EmployeeInfoModel findByMobile(String mobilenum);

	@Query("SELECT u FROM EmployeeInfoModel u WHERE u.id = ?1")
	EmployeeInfoModel findById(int id);
	
	


}
