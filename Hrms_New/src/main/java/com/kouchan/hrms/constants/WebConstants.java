package com.kouchan.hrms.constants;

/**
 * @author satish s k
 *
 */

public class WebConstants {
	// Rest messages
	public static final String FAILURE = "Failure";
	public static final String INTERNAL_SERVER_ERROR_CODE = "500";
	public static final String INTERNAL_SERVER_ERROR_MESSAGE = "internal server error";

	public static final String WEB_RESPONSE_SUCCESS = "SUCCESS";
	public static final String WEB_RESPONSE_FAILURE = "FAILURE";
	public static final String WEB_RESPONSE_ERORR = "ERROR";
	public static final String WEB_RESPONSE_NO_RECORD_FOUND = "DATA_DOES_NOT_EXIST";
	public static final String EMPLOYEE_DOESNOT_EXIST = "Employee does not exit with this number";
	public static final String CUSTOMER_DOESNOT_EXIST = "Customer does not exit with this number";
	public static final String DELEIVERY_BOY_DOESNOT_EXIST = "Delevery Boy Does't Exists";
	public static final String WEB_RESPONSE_ERROR = "Error";
	public static final String PASSWORD = "Invalid Password";
	public static final String OTP = "OTP verfied sucessfully ";
	public static final String PASSWORD_MISMATCH = "Password Mismatch";
	
}
