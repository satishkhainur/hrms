package com.kouchan.hrms.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.kouchan.hrms.constants.CommonConstants;
import com.kouchan.hrms.constants.WebConstants;
import com.kouchan.hrms.exception.BusinessException;
import com.kouchan.hrms.model.ChangePasswordModel;
import com.kouchan.hrms.model.EmployeeInfoModel;
import com.kouchan.hrms.model.EmployeeLoginRequest;
import com.kouchan.hrms.response.CommonResponse;
import com.kouchan.hrms.response.EmployeeInfoResponse;
import com.kouchan.hrms.service.EmployeeInfoService;
import com.kouchan.hrms.utils.RestUtils;
import com.mysql.jdbc.StringUtils;

@RestController
public class LoginController {

	private static final Logger LOG = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	EmployeeInfoService employeeInfoService;

	@PostMapping(value = "/hrms/registration")
	public CommonResponse employeeRegistration(@RequestBody EmployeeInfoModel employeeInfoModel )
	{
		CommonResponse response = null;
		EmployeeInfoModel newEmployee = null;
		if(employeeInfoModel!= null)
		{
			 newEmployee = employeeInfoService.save(employeeInfoModel);
		}
		EmployeeInfoResponse employeeResponse = new EmployeeInfoResponse();
		BeanUtils.copyProperties(newEmployee , employeeResponse);
		response = RestUtils.wrapObjectForSuccess("Employee Successfully registered");
		return response;
	}

		
	@RequestMapping(value = "/hrms/login", method = RequestMethod.POST)
	public CommonResponse login(@RequestBody EmployeeLoginRequest employeeLoginRequest) {
		CommonResponse response = null;

		Map<String, Object> map = validateLogin(employeeLoginRequest);
		if (!map.isEmpty()) {
			response = RestUtils.wrapObjectForFailure(map, "validation error", WebConstants.WEB_RESPONSE_ERORR);
			LOG.error("Login Validation missing");
		}
		try {

			EmployeeInfoModel employeeEixst = employeeInfoService.login(employeeLoginRequest);
			if (employeeEixst == null) {
				return response = RestUtils.wrapObjectForFailure(employeeEixst, "302",
						WebConstants.EMPLOYEE_DOESNOT_EXIST);
			}
			EmployeeInfoResponse employeeResponse = new EmployeeInfoResponse();
			BeanUtils.copyProperties(employeeEixst, employeeResponse);
			response = RestUtils.wrapObjectForSuccess(employeeResponse);
			LOG.info("Logged in successfully " + employeeResponse.getMobileNumber());
		} catch (BusinessException be) {
			response = RestUtils.wrapObjectForFailure(null, be.getErrorCode(), be.getErrorMsg());
		} catch (Exception e) {
			response = RestUtils.wrapObjectForFailure(null, WebConstants.WEB_RESPONSE_ERROR,
					WebConstants.INTERNAL_SERVER_ERROR_MESSAGE);
			e.printStackTrace();
		}
		return response;
	}

	// Validate Login Credentials
	private Map<String, Object> validateLogin(EmployeeLoginRequest employeeLoginRequest) {

		Map<String, Object> map = new HashMap<>();
		if (employeeLoginRequest == null) {
			map.put("All", CommonConstants.ALL_FIELDS_REQUIRED);
		} else {
			if (StringUtils.isNullOrEmpty(employeeLoginRequest.getMobileNumber())
					|| StringUtils.isEmptyOrWhitespaceOnly(employeeLoginRequest.getMobileNumber())) {
				map.put("mobileNumber", CommonConstants.MOBILE_NUMBER_EMPTY);
			}
			if (StringUtils.isNullOrEmpty(employeeLoginRequest.getPassword())
					|| StringUtils.isEmptyOrWhitespaceOnly(employeeLoginRequest.getPassword())) {
				map.put("password", CommonConstants.PASSWORD_EMPTY);
			}
		}
		return map;
	}

	@RequestMapping(value = "hrms/changePassword", method = RequestMethod.PUT)
	public CommonResponse newPassword(@RequestBody ChangePasswordModel changePasswordModel) throws Exception {
		CommonResponse response = null;
		EmployeeInfoModel EmpInfo = employeeInfoService.getByEmployeeId(changePasswordModel.getId());
		if (EmpInfo == null) {
			response = RestUtils.wrapObjectForFailure(changePasswordModel, "302", WebConstants.EMPLOYEE_DOESNOT_EXIST);
		} else {
			// String presentPassword = e.decrypt(merchantModel.getPassword());
			String oldPasswordfrmdb = EmpInfo.getPassword();
			String oldPassword = changePasswordModel.getOldPassword();
			String newPassword = changePasswordModel.getNewPassword();
			//String confirmPassword = changePasswordModel.getConfirmPassword();
			if (oldPasswordfrmdb.equals(oldPassword)) {
				EmployeeInfoModel empModel = employeeInfoService.updatePassword(EmpInfo, changePasswordModel);
			//	MerchantResponse merchantResponse = new MerchantResponse();
			//	BeanUtils.copyProperties(merchModel, merchantResponse);
				// merchantResponse.setPassword(e.decrypt(merchModel.getPassword()));
			//	merchantResponse.setPassword((merchModel.getPassword()));
				response = RestUtils.wrapObjectForSuccess("Successfully Password updated ");
			} else {
				response = RestUtils.wrapObjectForFailure(changePasswordModel, "402", WebConstants.PASSWORD);
			}
		}
		return response;
	}


}