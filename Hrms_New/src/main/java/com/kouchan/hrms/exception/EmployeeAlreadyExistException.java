package com.kouchan.hrms.exception;

import java.util.List;

import com.kouchan.hrms.coredata.CodeMessage;

public class EmployeeAlreadyExistException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private List<CodeMessage> errorMessages;

	public EmployeeAlreadyExistException() 
	{
		super();
	}

	public EmployeeAlreadyExistException(final List<CodeMessage> errorMessages) {
		this.errorMessages = errorMessages;
	}

	public List<CodeMessage> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(final List<CodeMessage> errorMessages) {
		this.errorMessages = errorMessages;
	}

	/**
	 * Instantiates a new user already exists exception.
	 *
	 * @param mobileNumber
	 *            the mobile number
	 */
	public EmployeeAlreadyExistException(final String mobileNumber) {
		super("Merchant already exists with  this mobile number: " + mobileNumber);
	}

}
