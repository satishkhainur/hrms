package com.kouchan.hrms.exception;



public enum ErrorCodes 
{
	MOEXISTS("Mobile already exists"),
	NOTSAVED("Not Saved"),
	MID("Merchant ID Not Found"),
	INVALIDOTP("Invalid otp"),
	UNFOUND("User not found, please register"),
	CNFOUND("Customer not found, please register"),
	SUCCESS("success"),
	FCMTOKEN("FCM Token Missing"),
	CATEGORY("Category is null"),
	IDNULL("Invalid ID"),
	PASSWORD("Enter Valid Password"),
	MOBNOTEXIST("ENTER VALID MOBILE NUMBER");
	
	
	private final String value;

	private ErrorCodes(String value) {
		this.value = value;
	}
	
	public String value() {
        return value;
    }

    public static ErrorCodes fromValue(String value) {
        for (ErrorCodes appCode : ErrorCodes.values()) {
            if (appCode.value.equals(value)) {
                return appCode;
            }
        }
        throw new IllegalArgumentException(value);
    }
}
