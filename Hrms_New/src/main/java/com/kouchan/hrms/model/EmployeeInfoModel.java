package com.kouchan.hrms.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "employee_info")
public class EmployeeInfoModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "emp_unique_id")
	private int empuniqueid;

	@Column(name = "emp_name")
	@NotEmpty(message = "Please provide Name")
	private String empName;

	@Column(name = "emp_father_name")
	@NotEmpty(message = "Please provide Father Name")
	private String empFatherName;

	@Column(name = "emp_surname")
	@NotEmpty(message = "Please provide SurName")
	private String empSurName;

	@Column(name = "emp_dob")
	@NotEmpty(message = "Please provide Date Of Birth")
	private String dob;

	@Column(name = "mobile_number")
	@NotEmpty(message = "Please provide mobile Number")
	private String mobileNumber;

	@Column(name = "email_id")
	@Email(message = "Please provide a valid e-mail")
	@NotEmpty(message = "Please provide an e-mail")
	private String emailId;

	@Column(name = "martial_status")
	private String martialStatus;

	@Column(name = "spouse_name")
	private String spouseName;

	@Column(name = "password")
	@NotEmpty(message = "Please provide Password")
	private String password;

	@Column(name = "present_address")
	private String presentAddress;

	@Column(name = "communication_address")
	private String communicationAddress;

	@Column(name = "nationality")
	private String nationality;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_on")
	private Date createdOn;
	

	public int getEmpuniqueid() {
		return empuniqueid;
	}

	public void setEmpuniqueid(int empuniqueid) {
		this.empuniqueid = empuniqueid;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getEmpFatherName() {
		return empFatherName;
	}

	public void setEmpFatherName(String empFatherName) {
		this.empFatherName = empFatherName;
	}

	public String getEmpSurName() {
		return empSurName;
	}

	public void setEmpSurName(String empSurName) {
		this.empSurName = empSurName;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMartialStatus() {
		return martialStatus;
	}

	public void setMartialStatus(String martialStatus) {
		this.martialStatus = martialStatus;
	}

	public String getSpouseName() {
		return spouseName;
	}

	public void setSpouseName(String spouseName) {
		this.spouseName = spouseName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPresentAddress() {
		return presentAddress;
	}

	public void setPresentAddress(String presentAddress) {
		this.presentAddress = presentAddress;
	}

	public String getCommunicationAddress() {
		return communicationAddress;
	}

	public void setCommunicationAddress(String communicationAddress) {
		this.communicationAddress = communicationAddress;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	
	
}
