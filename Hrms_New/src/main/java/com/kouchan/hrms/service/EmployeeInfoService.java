package com.kouchan.hrms.service;

import com.kouchan.hrms.model.ChangePasswordModel;
import com.kouchan.hrms.model.EmployeeInfoModel;
import com.kouchan.hrms.model.EmployeeLoginRequest;

public interface EmployeeInfoService {

	EmployeeInfoModel login(EmployeeLoginRequest employeeLoginRequest)throws Exception;

	EmployeeInfoModel save(EmployeeInfoModel employeeInfoModel);

	EmployeeInfoModel getByEmployeeId(int id);

	EmployeeInfoModel updatePassword(EmployeeInfoModel empInfo, ChangePasswordModel changePasswordModel);

}
