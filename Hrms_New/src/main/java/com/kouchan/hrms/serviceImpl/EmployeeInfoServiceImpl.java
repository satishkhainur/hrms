package com.kouchan.hrms.serviceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kouchan.hrms.Repo.EmployeeInfoRepo;
import com.kouchan.hrms.coredata.CodeMessage;
import com.kouchan.hrms.exception.BusinessException;
import com.kouchan.hrms.exception.EmployeeAlreadyExistException;
import com.kouchan.hrms.exception.ErrorCodes;
import com.kouchan.hrms.model.ChangePasswordModel;
import com.kouchan.hrms.model.EmployeeInfoModel;
import com.kouchan.hrms.model.EmployeeLoginRequest;
import com.kouchan.hrms.service.EmployeeInfoService;

@Service
public class EmployeeInfoServiceImpl implements EmployeeInfoService{
	
	@Autowired
	EmployeeInfoRepo employeeInfoRepo;

	@Override
	public EmployeeInfoModel login(EmployeeLoginRequest employeeLoginRequest)throws Exception {
		EmployeeInfoModel employeeInfo = null;
		String Mob = employeeLoginRequest.getMobileNumber();
		
		if(Mob != null)
		{
			employeeInfo = employeeInfoRepo.findByMobileNumberandPassword(employeeLoginRequest.getMobileNumber(), employeeLoginRequest.getPassword());

		} else {
			throw new BusinessException(ErrorCodes.PASSWORD.name(), ErrorCodes.PASSWORD.value());
		}
		return employeeInfo;

	
	}

	@Override
	public EmployeeInfoModel save(EmployeeInfoModel employeeInfoModel) {
		// TODO Auto-generated method stub
		String mobilenum= employeeInfoModel.getMobileNumber();
		
		EmployeeInfoModel empExit = employeeInfoRepo.findByMobile(mobilenum);
		if (empExit != null) {
			final List<CodeMessage> list = new ArrayList<>();
			list.add(new CodeMessage("302", "Merchant alredy Exist with this mobile number",
					mobilenum));
			throw new EmployeeAlreadyExistException(list);
		}
					employeeInfoModel.setCreatedOn(new Date(System.currentTimeMillis()));
					return employeeInfoRepo.save(employeeInfoModel);
	}

	@Override
	public EmployeeInfoModel getByEmployeeId(int id) {
		return employeeInfoRepo.findById(id);
	}

	@Override
	public EmployeeInfoModel updatePassword(EmployeeInfoModel empInfo, ChangePasswordModel changePasswordModel) {
		// TODO Auto-generated method stub
		EmployeeInfoModel emp;
		int m2 = changePasswordModel.getId();
		emp = getByEmployeeId(m2);
		//m1.setPassword(e.encrypt(changePasswordModel.getNewPassword()));
		emp.setPassword((changePasswordModel.getNewPassword()));
		
		return employeeInfoRepo.save(emp);
	}

}
